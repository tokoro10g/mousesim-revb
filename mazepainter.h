#ifndef MAZEPAINTER_H
#define MAZEPAINTER_H

#include <QGraphicsScene>
#include "libmazesolver/maze.h"

class MazePainter
{
public:
    static const int graphics_width;
public:
    MazePainter();
    MazePainter(QGraphicsScene *_scene,MazeSolver::Maze *_maze,QColor _color);
    ~MazePainter();
    void reloadMaze();
    void draw() const;

    qreal getWidth() const { return step*(maze->getWidth()); }
    qreal getHeight() const { return step*(maze->getHeight()); }
private:
    QGraphicsScene *scene;
    QColor color;
	MazeSolver::Maze *maze;
    int step;
};

#endif // MAZEPAINTER_H
