#include "mazeloader.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

using namespace MazeSolver;

void MazeLoader::load(const QString &filepath, Maze &maze){
    QFile file(filepath);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return;
    }
    QTextStream in(&file);

    int type,w,h;
    in>>type;
    in>>w>>h;
    maze.resize(w,h);
    maze.setType(type);
    if(type==1){
        int x,y;
        in>>x>>y;
        maze.setGoal(x,y);
    } else {
        maze.setGoal(7,7);
    }
    for(int i=0;i<h;i++){
        for(int j=0;j<w;j++){
            char ch;
            CellData cell={0};
            in>>ch;
            if(ch>='0'&&ch<='9'){
                cell.half=ch-'0';
            } else if(ch>='a'&&ch<='f'){
                cell.half=ch+0xa-'a';
            } else if(ch==' '||ch=='\n'||ch=='\r'){
                j--;
                continue;
            } else {
                QMessageBox mb;
                mb.setText("Invalid maze data");
                mb.exec();
                //std::cerr<<"Invalid maze data"<<std::endl;
                return;
            }
            maze.setCell((h-i-1)*w+j,cell);
        }
    }
    file.close();
}

void MazeLoader::loadEmpty(int w,int h,int x,int y,Maze &maze){
    maze.resize(w,h);
    maze.setType(1);
    maze.setGoal(x,y);
    for(int i=0;i<h;i++){
        for(int j=0;j<w;j++){
            CellData cell={0};
            if(j==0){
                cell.bits.WEST=1;
            } else if(j==w-1){
                cell.bits.EAST=1;
            }
            if(i==0){
                cell.bits.SOUTH=1;
            } else if(i==h-1){
                cell.bits.NORTH=1;
            }
            if(i==0&&j==0){
                cell.bits.EAST=1;
				cell.bits.CHK_EAST=1;
            }
            if(i==0&&j==1){
                cell.bits.WEST=1;
				cell.bits.CHK_WEST=1;
            }
            maze.setCell(i*w+j,cell);
        }
    }
}
