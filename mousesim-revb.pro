#-------------------------------------------------
#
# Project created by QtCreator 2014-01-01T16:27:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mousesim-revb
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    stopwatch.cpp \
    mazepainter.cpp \
    libmazesolver/maze.cpp \
    mazeloader.cpp \
    myqgraphicsview.cpp \
    mazesimulator.cpp \
    libmazesolver/graph.cpp \
    graphpainter.cpp \
    libmazesolver/agent.cpp \
    agentpainter.cpp

HEADERS  += mainwindow.h \
    mazepainter.h \
    libmazesolver/maze.h \
    mazeloader.h \
    myqgraphicsview.h \
    mazesimulator.h \
    libmazesolver/graph.h \
    graphpainter.h \
    stopwatch.h \
    libmazesolver/agent.h \
    pointermanager.h \
    agentpainter.h

FORMS    += mainwindow.ui

CONFIG += c++11
