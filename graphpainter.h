#ifndef GRAPHPAINTER_H
#define GRAPHPAINTER_H

#include <QGraphicsScene>
#include "libmazesolver/graph.h"

class GraphPainter
{
	public:
		static const int graphics_width;
	public:
		GraphPainter();
		GraphPainter(QGraphicsScene *_scene,MazeSolver::Graph *_g);
		~GraphPainter();
		void reloadGraph();
		void drawGraph() const;
        void drawRoute(const MazeSolver::Route& route) const;

		qreal getWidth() const { return step*(g->getWidth()); }
		qreal getHeight() const { return step*(g->getHeight()); }
	private:
		QGraphicsScene *scene;
		MazeSolver::Graph *g;
		int step;
};

#endif // GRAPHPAINTER_H
