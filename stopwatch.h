#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

class StopWatch{
	private:
#ifdef _WIN32
    volatile DWORD begin;
    volatile DWORD end;
#else
		double begin;
        double end;
		double get_dtime() const;
#endif
	public:
		StopWatch():begin(0.0),end(0.0){}
		~StopWatch(){}
		void start();
		void stop();
        double getDuration() const;
		void show() const;
};
