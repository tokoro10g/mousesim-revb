#ifndef MAZELOADER_H
#define MAZELOADER_H

#include <QFile>

#include "libmazesolver/maze.h"

class MazeLoader
{
private:
    MazeLoader();
public:
    static void load(const QString &_filepath,MazeSolver::Maze &maze);
    static void loadEmpty(int w,int h,int x,int y,MazeSolver::Maze &maze);
};

#endif // MAZELOADER_H
